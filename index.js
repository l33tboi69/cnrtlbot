const rp = require('request-promise');
const cheerio = require('cheerio');
const TeleBot = require('telebot');
const bot = new TeleBot('1091878158:AAG03Qzmdu508bRuhyHMR6Q_C2ApmzzOKDs');

const BASE_URI = "https://www.cnrtl.fr/definition/"

const options = {
	transform: (body) => {
		return cheerio.load(body);
	}
};

const get_definition = (word) => {
	return rp({
		...options,
		uri: BASE_URI + encodeURI(word) 
	})
	.then($ => {
		return {
			definition: $('.tlf_cdefinition').first().text(),
			exemple: $('.tlf_cexemple').first().text()
		}
	})
	.catch(err => {
		console.error(err);
	});
}

bot.on('text', (msg, props) =>  {
	//console.log(props);
	const mot = msg.text;
	get_definition(mot)
	.then(data => {
		return bot.sendMessage(msg.chat.id, data.definition + '\n' + data.exemple);
	})
	.catch(err => {
		console.error(err);
	});
});

bot.start();
